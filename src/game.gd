extends Node2D

export(float) var player_speed
export(NodePath) var player_node_path

var player_node: KinematicBody2D = null

onready var _bullet_scene = preload("res://components/bullet.tscn")

# Called when the node enters the scene tree for the first time.
func _ready():
	player_node = get_node(player_node_path)

func move(delta: float)-> KinematicCollision2D :
	var horizontal: float = (
		Input.get_action_strength("move_right")
		- Input.get_action_strength("move_left")
	)
	
	var vertical: float = (
		Input.get_action_strength("move_down") 
		- Input.get_action_strength("move_up")
	)
	
	var movement: Vector2 = Vector2(horizontal, vertical) * delta * player_speed
	return player_node.move_and_collide(movement)

func shoot(delta: float):
	
	var bullet = null
	if Input.is_action_just_pressed("shoot_green"):
		print("green")
		bullet = _bullet_scene.instance()
		bullet.set_color(bullet.COLOR.green)
	if Input.is_action_just_pressed("shoot_blue"):
		print("blue")
		bullet = _bullet_scene.instance()
		bullet.set_color(bullet.COLOR.blue)
	if Input.is_action_just_pressed("shoot_yellow"):
		print("yellow")
		bullet = _bullet_scene.instance()
		bullet.set_color(bullet.COLOR.yellow)
	if Input.is_action_just_pressed("shoot_red"):
		print("red")
		bullet = _bullet_scene.instance()
		bullet.set_color(bullet.COLOR.red)

	if (bullet):
		add_child(bullet)
		bullet.global_position = player_node.global_position - Vector2(0, 40)


func _process(delta:float):
	move(delta)
	shoot(delta)
	