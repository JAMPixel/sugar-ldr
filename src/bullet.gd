extends KinematicBody2D

enum COLOR {
	yellow,
	red,
	blue,
	green,
	white
}

var color = COLOR.white

var speed: Vector2 = Vector2(0, -2400)


signal collide

func _ready():
	pass # Replace with function body.
	
func set_color(color_to_set):
	var polygon_color: Color = Color(255, 255, 255)
	color = color_to_set
	match(color):
		COLOR.yellow:
			polygon_color = Color("FFDF4C")
		COLOR.red:
			polygon_color = Color("FF4447")
		COLOR.blue:
			polygon_color = Color("4978FF")
		COLOR.green:
			polygon_color = Color("0FC47A")
	
	$Polygon2D.color = polygon_color

func move(delta: float) -> KinematicCollision2D:
	return move_and_collide(speed * delta)

func collide(collisions: KinematicCollision2D, delta: float):
	var collider = collisions.get_collider()
	if collider.has_method("hit_by_bullet"):
		collider.hit_by_bullet(color)
	get_parent().remove_child(self)
	
func _process(delta: float):
	var collisions = move(delta)
	if (collisions):
		collide(collisions, delta)