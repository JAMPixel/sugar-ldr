extends StaticBody2D

enum COLOR {
	yellow,
	red,
	blue,
	green,
	white
}

export(COLOR) var color;

func _ready():
	set_color(color)


func set_color(color_to_set):
	var polygon_color: Color = Color(255, 255, 255)
	match(color):
		COLOR.yellow:
			polygon_color = Color("FFDF4C")
		COLOR.red:
			polygon_color = Color("FF4447")
		COLOR.blue:
			polygon_color = Color("4978FF")
		COLOR.green:
			polygon_color = Color("0FC47A")
	
	$Polygon2D.color = polygon_color
	
func hit_by_bullet(bullet_color):
	
	if(bullet_color == color):
		get_parent().remove_child(self)
	